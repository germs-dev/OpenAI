---
title: Finance Matters
subtitle: ... with a little help from our OpenAI friends
author: Dean Turpin
---

{humerous finance quote}

## Random saying about trading

{random saying about trading}

## Kernel bypass

{Tell me about kernel bypass}

## Limit orders

{Describe limit orders in finance}

## List of common trading strategies

{List of common trading strategies}

## HFT firms

{list of top ten hft firms and links to their websites}

### Alternative platforms

{List of alternative trading platforms to Reactive Markets Switchboard}

## Quantative trading

{Describe quantative trading}

## Algorithmic trading

{Describe algorithmic trading}

There are three major blocks in an Algo Trading System:

1. Market Data Handler (e.g., FAST handler)
1. Strategy Module (e.g., crossOver strategy)
1. Order Router (e.g., FIX router)

### High-frequency trading (HFT)

{Describe high-frequency trading}

- Sophisticated algorithms
- Co-location
- Short-term investment horizons
- Sophisticated technological tools and computer algorithms to rapidly trade securities
- Proprietary trading strategies carried out by computers to move in and out of positions in seconds or fractions of a second

## Day trading

{Describe day trading}

## Market makers

{Describe market makers}

## Financial instruments

{Markdown list of financial instruments}

### Derivatives

{Describe derivatives financial instruments}

## Bid-ask spread

{Describe bid-ask spread}

- Me: BUY/SELL
- Them: OFFER/BID

## Trading protocols

- FIX: [Financial Information eXchange](https://en.wikipedia.org/wiki/Financial_Information_eXchange)
- OUCH: a digital communications protocol that allows customers of the NASDAQ (National Association of Securities Dealers Automated Quotations) to conduct business in the options market.
- [SAIL](https://en.wikipedia.org/wiki/Sola_Access_Information_Language)
- Millenium - LSE

## Buy/sell side

{Compare buy side and sell side in trading}

### Which side are these?

- Economists
- Strategists
- Providing research
- Standard chartered: retail presence (taking deposits from consumers)
- Old school: give a loan against deposit
- Algorithmic trading
- IPO
- Issue bond to bunch of investors
- Small investment managers
- Options pricing model

## Challenger banks

{Compare challenger banks and investment banks}

## Trading platforms

{markdown list of computer trading platforms}


