openai_request () {

        local question=$*

        local request="{ \
                \"model\": \"text-davinci-003\",
                \"prompt\": \"$question\"
                \"temperature\": 0.7,
                \"max_tokens\": 100,
                \"top_p\": 1,
                \"frequency_penalty\": 0.0,
                \"presence_penalty\": 0.0
                }"

	# Construct the prompt
        local req="Things to do in Brighton"
        local brighton="
          \"model\": \"text-davinci-003\",
          \"prompt\": \"$question\",
          \"temperature\": 0.7,
          \"max_tokens\": 100,
          \"top_p\": 1,
          \"frequency_penalty\": 0.0,
          \"presence_penalty\": 0.0
          "

	# Issue the request
        local response=$(curl https://api.openai.com/v1/completions \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $OPENAI_API_KEY" \
	  -d "{$brighton}")

	# echo $response
	# echo
	# echo
	# echo

        # Parse the response
	[[ $response =~ \"text\":\"(.*)\",\"index\" ]] && echo -e "${BASH_REMATCH[1]}" || echo no match
}

cat README.md | while read line; do
        if [[ $line =~ ^\{(.*)\}$ ]]; then
		# echo $(openai_request ${BASH_REMATCH[1]}) || echo $line
		# echo match
		prompt=${BASH_REMATCH[1]}
		request="
		  \"model\": \"text-davinci-003\",
		  \"prompt\": \"$prompt\",
		  \"temperature\": 0.7,
		  \"max_tokens\": 100,
		  \"top_p\": 1,
		  \"frequency_penalty\": 0.0,
		  \"presence_penalty\": 0.0
		  "
		# echo $request

		# Issue the request
		response=$(curl https://api.openai.com/v1/completions \
		  -H "Content-Type: application/json" \
		  -H "Authorization: Bearer $OPENAI_API_KEY" \
		  -d "{$request}")

		# Parse the response
		# echo -e $response
		[[ $response =~ \"text\":\"(.*)\",\"index\" ]] && echo -e "${BASH_REMATCH[1]}" || echo Computer says no

        else
		# If it's not a query then just print the line
		echo $line
	fi
done
