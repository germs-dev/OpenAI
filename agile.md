---
title: Scrum in 1500 words
subtitle: ... without overthinking it
author: Dean Turpin
---

{markdown happy dog gif}

This is me collecting my thoughts after being involved in setting up a few scrum teams and for the most part feeling disappointed. I've actually yet to experience pure Agile as it has without fail been subverted to fit into existing processes.

http://agilemanifesto.org/principles.html

_tl;dr: only implement the things you need to do the job_

This is not a comprehensive scrum guide -- see [scrum.org](https://www.scrum.org/) -- but a list of the important things having done it a few times.

What does OpenAI think Agile is?

{Describe the principles of the Agile design methodology?}

### The interminable cycle
- Plan: agree your goals select issues that support those goals
- Sprint: do the work
- Review: demo what you did
- Retro: what went well or could be better?
 
```mermaid
graph
    sprint_planning --> sprint --> sprint_review --> sprint_retrospective --> sprint_planning
```

### Roles
- Dev Team: developers
- Product Owner: not a dev
- Scrum Master: developer but not the PO

### Sprint planning
- Choose your sprint goals and then find all the related tickets: it's much more efficient to complete a bunch of similar tickets as a group
- If a ticket is not ready or blocked then it doesn't go in the sprint

Avoid the temptation to plan out the next few sprints, inevitably you just end up pushing things to the next sprint in a bow wave of "we'll definitely get around to this next sprint."

### Estimation
Use complexity rather than time based estimation. Humans are much better at comparing the size of tasks than how long they will take. And if you make it about complexity you don't get into the craziness of weighting tasks depending on who's doing it (which should be anybody.) Estimate using Fibonnaci numbers so you don't dwell on whether something should be 4, 5 or 6. Resist the temptation to think of complexity as time.

- 1 - Update a markdown file
- 2 - Simple change in compiled code: e.g., add an `if` statement around existing code
- 3 - Update a single function; remove commented code from a whole file
- 5 - Remove redundant code from a whole file
- 8 - Establish network connection between two objects using existing middleware
- 13 - Consider breaking down the task

At the end of each sprint, tabulate issue summary: total tickets completed, total complexity points claimed, maximum complexity. There's no point adding issues larger than your maximum completed complexity.

#### Re-estimation
If you haven't finished a ticket, __no points are claimed__; you shouldn't be splitting tickets at the end of a sprint just to claim some points: it's obscuring the fact that you haven't broken down tickets enough.

People usually don't feel comfortable "losing out" on incomplete issues and carry over points into the next sprint. Which just makes a mockery of the velocity: you end up justifying wildly over-committing because you want to claim the ten points for that issue that never gets completed. Also, you've no idea what the maximum story you can take into a sprint is. Whereas if you keep chipping away and re-estimating, at some point you'll complete it at a size that fits into a sprint.

### Velocity
It's interesting to see the total story points completed -- the velocity -- but also it might be interesting to histogram the complexities: what's the maximum complexity ticket completed in a sprint?

Velocity is a metric for how good you are at completing software work, not tracking the passage of time. If the velocity has dropped then it should be discussed at the retrospective: don't add tickets to keep the numbers up.

You shouldn't be adding tickets for annual leave or meetings: they are not dev work.

## List the important takehomes
- Strict start/stop date
- Don't split tasks just so you can claim a bit: no cooking the books!

## Misunderstandings
### Agile means you can quickly switch to doing something else
Agile means you can quickly decide to __not bother doing something at all__.

### You can add annual leave tasks so we don't lose velocity
Velocity is a measure of how well you define and complete tasks, not the inextricable passage of time (which you can't improve.)

## Complexity-based estimation
It's incredibly difficult to get people out of the habit of time-based estimation. You often get into a discussion during planning about whether a task should be weighted because a junior is going to do it. So not only have you pre-assigned a task, but if a senior picks it up do they claim twice the points? Complexity makes it task-based and removes all that craziness.

When breaking down a story I quite like to start with a minimal complexity sub-task and break down the remaining work relative to it. And I think a good minimal complexity task is editing a standalone text file.

Low complexity tasks are less likely to hit anything unexpected.

### Sprint zero
Trial run.

### Product backlog
The `product backlog` is a living artefact, higher ranked items should be more
refined. It is a placeholder for a conversation.

Each refined story in the sprint must have a weight/complexity for planning
purposes. These estimates must be done by the people actually doing the work.
They should also have a business value and the `product owner` must share a
clear objective/vision for the forthcoming sprint.

## Estimating
Humans are bad at estimating how long will take. But they are good at comparing the complexity of tasks: is task two more complex than task one? Use complexity points to move away from thinking about duration.

Use the Fibonacci sequence so you don't overthink it: is it 2.5 or 3 complexity points?

> Is this piece of work larger or smaller than this other thing we did last week?

Estimate against work that's already been performed. New backlog? Estimate
against a known thing. It might be useful to build up and maintain a list of
example tasks and complexities.

```
1. Change in a plain text file with no dependencies (a markdown doc)
2. Change in a plain text file which makes things happen (.gitlab-ci.yml)
3. One line change in a compiled file.
5. Multiple line change in a compiled file.
8. Create new Visual Studio project.
```

## Daily scrum

Remember that the daily scrum is not a status meeting for the product owner. They can attend but will often become the focus of attention and naturally end up driving the order of proceedings. 

it's a planning meeting for the dev team. It's also intended for the dev team
so doesn't necessarily include the `scrum master` of `product owner`. Don't
look at the `scrum master`, look at the board.

- What did I do yesterday that…
- What will I do today that…
- Do I see any impediment that prevents me or the team from…

## Scrum master
The scrum master's prime responsibility is removing impediments. Learn to
navigate the politics of your organisation. A dead scrum master is a useless
scrum master. `scrum master` is responsible for success of current sprint,
`product owner` is responsible for the planning of upcoming sprints. `scrum
master` is also responsible for ensuring that planning occurs.

## Meeting the sprint goal
- Teams who manually update their burn down chart show more awareness of how they are pacing toward their goal.
- Invest 10 % of time in refining of backlog. Nice Friday afternoon activity.  Not critical that they occur on the same day.
- Wednesday is a good start sprint day.
- Scope bound versus time bound.
- Burn up isn't part of Scrum framework but is effective.
- The `product owner` is the only one who sets the direction of the team.

## Sprint Review
The sprint review is not the PO's chance to review work done during the Sprint. It gives stakeholders the opportunity to give feeback on the state and direction of the product. And collaborate on the goals for the next sprint.

Remove rough estimates from backlog prior to sprint planning.

- Efficiency versus effectiveness?
- Cross-functional dev team
- How to introduce Agile gradually
- Planning poker
- Create reference items so the team has something to compare to
- Predictability is the goal, not velocity

## Customer
- Frequent delivery with client feedback.
- Script the manual installation procedure.

## Agile methodologies
- XP
- Produce delivery of software at the end of each scrum
- Who is the customer?
- Feature list? Action > result > object

## User stories
```
As a <role> I want <feature> so that <benefit>.
Given <context> when <something happens> then <desired result>.
```

Encourage simplicity: don't put the implementation details in the requirement.

## Sprint review
- Schedule demo of actual software on representative hardware.
- Use comments to direct the next batch of work.

## How long is an epic?
- Theme - multi-year
- Epic - 6 months to 1 year
- Feature - One quarter or less
- Story - One Sprint or less

https://www.scrum.org/forum/scrum-forum/16650/best-practice-size-epics

## References
- [Why you should burn your backlog now](https://thenewstack.io/mary-poppendieck-on-why-you-should-just-burn-your-backlog/)
- [Redefining software engineering](https://www.infoq.com/presentations/redefining-software-engineering/)

