import json
import requests
import os

# Read API key from environment variable
openai_api_key = os.environ.get("OPENAI_API_KEY")


def openai_request(question):
    api_key = openai_api_key
    model = "gpt-4"
    temperature = 0.7
    max_tokens = 100
    top_p = 1
    frequency_penalty = 0.0
    presence_penalty = 0.0
    prompt = question

    payload = {
        "model": model,
        "prompt": prompt,
        "temperature": temperature,
        "max_tokens": max_tokens,
        "top_p": top_p,
        "frequency_penalty": frequency_penalty,
        "presence_penalty": presence_penalty
    }

    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + api_key
    }

    response = requests.post("https://api.openai.com/v1/completions", json=payload, headers=headers)
    response_data = response.json()

    if "choices" in response_data and len(response_data["choices"]) > 0:
        return response_data["choices"][0]["text"]
    else:
        return "Computer says no"

with open("README.md", "r") as file:
    lines = file.readlines()

for line in lines:
    line = line.strip()
    if line.startswith("{") and line.endswith("}"):
        prompt = line[1:-1]
        completion = openai_request(prompt)
        print(completion)
    else:
        print(line)
